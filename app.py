import json
import os
import connexion
from apscheduler.schedulers.background import BackgroundScheduler
from connexion import NoContent
import yaml
import logging
from logging import config
import datetime
import requests
from flask_cors import CORS, cross_origin

# Your functions here to handle your endpoints
if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

logger.info("Assignment 3")

def get_stats():
    """ Gets new blood pressure readings after the timestamp """
    logger.info("Start Get Stats Request")
    logger.info("Get Stats Request Has Started.")
    try:
        with open(app_config["datastore"]["filename"], 'r') as file:
            data = json.load(file)
        logger.debug(f"Statistics are: {data}")

    except FileNotFoundError:
        logger.error("404 - Statistics do not exist.")

    logger.info("Get Stats Request Has Completed.")
    return data, 200


def populate_stats():
    """ Periodically update stats """
    logger.info("Start Periodic Processing")
    logger.info("Periodic Processing Has Started.")
    read_from_json()
    rest_record = get_book_records(app_config["eventstore"]["url"])
    review_record = get_item_records(app_config["eventstore"]["url"])
    calc_stat(rest_record, review_record)
    # print(calc_stat(rest_record, review_record))
    logger.debug("Periodic Processing has ended!")

    return NoContent, 200


def read_from_json():
    offset_time = datetime.datetime.now() - datetime.timedelta(minutes=15)
    last_updated = offset_time.strftime("%Y-%m-%dT%H:%M:%SZ")

    # current_time = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    try:
        with open(app_config["datastore"]["filename"], 'r') as file:
            file = json.load(file)

    except FileNotFoundError:
        default_file = {"count_bb_book_name": 0,
                        "max_ri_item_reserved": 0,
                        "last_updated": last_updated}
        default_json_file = json.dumps(default_file, indent=4)
        with open(app_config["datastore"]["filename"], 'w') as file:
            file.write(default_json_file)

    return file


def write_to_json(book, item):
    current_time = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    stat_data = {"count_bb_book_name": book,
                 "max_ri_item_reserved": item,
                 "last_updated": current_time}

    record_json = json.dumps(stat_data, indent=4)

    with open(app_config["datastore"]["filename"], 'w') as file:
        file.write(record_json)
        # print(file)

    return file


def get_book_records(s_url):
    headers = {"Content-Type": "application/json"}
    current_datetime = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    response = requests.get(
        f'{s_url}books/borrow_your_book?start_timestamp={read_from_json()["last_updated"]}'
        f'&end_timestamp={current_datetime}', headers=headers)

    json_data_borrow_your_book = json.loads(response.text)

    logger.info(
        f'Query for book search records between {read_from_json()["last_updated"]} and {current_datetime} '
        f'returns {len(json_data_borrow_your_book)} results.')

    if not response.status_code // 100 == 2:
        logger.debug(f'Query for review [ERROR]: Error: Unexpected response [{response.status_code}]: {response}.')

    return json_data_borrow_your_book


def get_item_records(s_url):
    headers = {"Content-Type": "application/json"}
    current_datetime = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    response = requests.get(
        f'{s_url}books/reserve_your_item?start_timestamp={read_from_json()["last_updated"]} '
        f'&end_timestamp={current_datetime}', headers=headers)

    json_data_reserve_your_item = json.loads(response.text)
    logger.info(
        f'Query for review records between {read_from_json()["last_updated"]} and {current_datetime} '
        f'returns {len(json_data_reserve_your_item)} results.')

    if not response.status_code // 100 == 2:
        logger.debug(f'Query for restaurant [ERROR]: Error: Unexpected response [{response.status_code}]: {response}.')

    return json_data_reserve_your_item


def calc_stat(book_data, review_data):
    """ Calculate stats for restaurant searched records filtered by timestamp """
    book_name_counter = 0
    item_reserved_counter = 0

    #    data = read_from_json()
    #    if "book_name" in data.keys():
    #        data["book_name"] += len(book_data)
    #    else:
    #        data["book_name"] = len(book_data)

    #    if "item_type" in data.keys():
    #        data["item_type"] += len(review_data)
    #    else:
    #        data["item_type"] = len(review_data)

    for record in book_data:
        if record["book_name"] == "Six of Crows":
            book_name_counter += 1

    for record in review_data:
        # print(record)
        if record["item_type"] == "computer":
            item_reserved_counter += 1

    if item_reserved_counter > 0 and book_name_counter > 0:
        write_to_json(book_name_counter, item_reserved_counter)
    return [book_data, review_data]


def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats, 'interval', seconds=app_config['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content_Type'
app.add_api("openapi.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    # populate_stats()
    # run our standalone gevent server
    init_scheduler()
    app.run(port=8100, host="0.0.0.0", use_reloader=False)
